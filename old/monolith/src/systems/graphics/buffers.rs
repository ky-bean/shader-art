use super::Vertex;

// TODO: Is this still needed?
pub(crate) struct Buffers {
	pub vertex: wgpu::Buffer,
	pub index: wgpu::Buffer,
}
impl Buffers {
	pub fn new(vertex: wgpu::Buffer, index: wgpu::Buffer) -> Self {
		Self { vertex, index }
	}
}

// TODO: Find a better name for this
#[derive(Debug)]
pub(crate) struct RawBuffer {
	pub vertices: &'static mut [Vertex],
	pub indices: &'static mut [u16],
}
impl RawBuffer {
	pub fn new(
		vertices: &'static mut [Vertex],
		indices: &'static mut [u16],
	) -> Self {
		Self { vertices, indices }
	}
}
