use std::sync::mpsc;

use analysis::SpectrumProcessor;
use cpal::traits::{DeviceTrait, HostTrait};
use input::{CpalStreamErrorHandler, CpalStreamHandler};
use spectrum_analyzer::{Frequency, FrequencySpectrum, FrequencyValue};
use tracing::{debug, info, trace};
use tracing_unwrap::{OptionExt, ResultExt};

use self::input::cpal_handler::InputPayload;
pub(crate) use self::signal_params::SignalParams;

/// The data type representing the values for samples of audio
type Sample = f32;
/// A buffer of [`Sample`]s
type SamplesBuf = Vec<Sample>;

mod analysis;
mod input;

mod signal_params;

// TODO: Add some way to detect if/when the threads lose real-time execution

pub(crate) struct AudioSystem {
	audio_input_stream: cpal::Stream,
	spectrum_data: mpsc::Receiver<FrequencySpectrum>,
	signal_params: SignalParams,
}

impl AudioSystem {
	/// Construct the audio system
	///
	/// Spawns two new threads: one to take in samples from the system audio
	/// input source and one to calculate the Fourier transfrom of the samples.
	#[tracing::instrument(name = "audio_system")]
	pub(crate) fn new() -> Self {
		let (samples_sender, samples_receiver) = mpsc::channel();
		let (config, audio_input_stream) =
			input::cpal_utils::init_input_stream(samples_sender);
		let (spectrum_sender, spectrum_receiver) = mpsc::channel();
		Self::init_spectrum_analyzer(
			samples_receiver,
			spectrum_sender,
			config.sample_rate.0,
		);

		Self {
			audio_input_stream,
			spectrum_data: spectrum_receiver,
			signal_params: Default::default(),
		}
	}

	#[tracing::instrument(name = "audio_system:update", skip(self))]
	pub(crate) fn update(&mut self) {
		let mut spectra = Vec::new();

		loop {
			match self.spectrum_data.try_recv() {
				Ok(spectrum) => {
					spectra.push(spectrum);
				}
				Err(mpsc::TryRecvError::Empty) => break,
				Err(mpsc::TryRecvError::Disconnected) => {
					// TODO: Notify the engine that the audio system has shut
					// down and that it probably should too, I think. (Event
					// system?)
					break;
				}
			}
		}

		if spectra.is_empty() {
			return;
		}

		trace!(
			"Received {} spectrum frames from the spectrum processor",
			spectra.len()
		);

		self.signal_params = SignalParams::new(&Self::spectra_mean(&spectra));
	}

	pub(crate) fn signal_params(&self) -> SignalParams {
		self.signal_params
	}

	fn spectra_mean(spectra: &[FrequencySpectrum]) -> FrequencySpectrum {
		let frequency_resolution = spectra[0].frequency_resolution();
		let samples_len = spectra[0].samples_len();
		let mut working_buffer =
			vec![
				(Frequency::from(0.0), FrequencyValue::from(0.0));
				samples_len as usize
			];

		let mut avg_spectrum = working_buffer.clone();

		let total = spectra.len() as f32;
		trace!(
			frequency_resolution,
			samples_len,
			total,
			"Calculating the average spectrum from the collected spectra"
		);

		for spectrum in spectra.iter() {
			for (i, freq_val) in spectrum.data().iter().enumerate() {
				avg_spectrum[i].0 =
					avg_spectrum[i].0 + (freq_val.0 / Frequency::from(total));
				avg_spectrum[i].1 = avg_spectrum[i].1
					+ (freq_val.1 / FrequencyValue::from(total));
			}
		}

		FrequencySpectrum::new(
			avg_spectrum,
			frequency_resolution,
			samples_len,
			&mut working_buffer,
		)
	}

	fn init_spectrum_analyzer(
		receiver: mpsc::Receiver<InputPayload>,
		sender: mpsc::Sender<FrequencySpectrum>,
		sampling_rate: u32,
	) {
		let spectrum_processor = SpectrumProcessor::new(
			receiver,
			sender,
			sampling_rate,
			// TODO: Probably will want to have some scaling function at some
			// point in the future, but none is ok for now.
			None,
		);
		info!("Starting thread for spectrum processing");
		std::thread::spawn(move || spectrum_processor.processor_thread());
	}

	// TODO: Continue this implementation:
	// - `signal_analyzer` should run in a thread, which recieves samples from
	// the audio thread by holding 1sec worth of audio in a FIFO queue and
	// provides a window of size 16384 (maybe see if `signal_analyzer` can go
	// higher?) centered at the current frame's timestamp.
	// - need to figure out how to translate timestamps from `cpal` into
	// `Instant`s or something I can compare with the values I can get from
	// `crate::time::TimeTracker`.
}

// TODO: Take the findings from this and implement `spectrum-analyzer` locally
// and *correctly* (summing the halves of the dft to get the full value (doesn't
// have to be a sum, it can just be "double every bucket except bucket 0 and
// bucket N/2")).
//
// TODO: Maybe also reimplement complex numbers to be r-theta instead of re-im,
// but idk if I'll need complex outside of tests like this
#[cfg(test)]
mod test {
	use std::f32::consts::PI;

	use num_complex::Complex32;
	use spectrum_analyzer::FrequencyLimit;

	#[test]
	fn audio_pipeline() {
		todo!()
	}

	fn basic_dft(
		signal: impl Fn(f32) -> f32,
		num_samples: usize,
		sample_rate: f32,
	) -> Vec<f32> {
		let mut dft = Vec::new();

		for n in 0..num_samples {
			let n = n as f32;
			let mut bin = Complex32::new(0.0, 0.0);
			for k in 0..num_samples {
				let k = k as f32;
				bin += Complex32::from_polar(
					signal(k / sample_rate),
					2.0 * PI * n * k / (num_samples as f32),
				);
			}
			let (r, theta) = bin.to_polar();
			let is_negative = theta <= -PI / 2.0 || theta >= PI / 2.0;
			let r = if is_negative { -r } else { r };
			dft.push(r);
		}

		dft
	}

	use super::*;
	#[test]
	fn spectrum_thinking() {
		// dc: 5
		// 1Hz: 2
		// 2Hz: 3
		// 3Hz: 1
		// 4Hz: 4
		let signal = |t: f32| {
			5.0 + 2.0 * (2.0 * PI * t - PI / 2.0).cos()
				+ 3.0 * (4.0 * PI * t).cos()
				+ (6.0 * PI * t + PI / 2.0).cos()
				+ 4.0 * (8.0 * PI * t - PI).cos()
		};

		let basic_dft = basic_dft(signal, 8, 8.0);
		dbg!(basic_dft);

		// let samples = [signal(0.0), signal(0.25), signal(0.5), signal(0.75)];
		// let spectrum = spectrum_analyzer::samples_fft_to_spectrum(
		// 	&samples,
		// 	4,
		// 	FrequencyLimit::All,
		// 	None,
		// )
		// .unwrap();
		// dbg!(spectrum);
	}
}
