use spectrum_analyzer::{Frequency, FrequencySpectrum, FrequencyValue};

#[derive(Default, Debug, Copy, Clone, PartialEq, PartialOrd)]
pub(crate) struct SignalParams {
	energy: f32,
	// 0Hz < f < 100Hz
	user1: f32,
	// 200Hz < f < 800Hz
	user2: f32,
	// 1,000Hz < f < 8,000Hz
	user3: f32,
	// 10,000Hz < f < 20,000Hz
	user4: f32,
}

fn split_freqs(
	spectrum_data: &[f32],
	frequency_resolution: f32,
	lower_freq: f32,
	upper_freq: f32,
) -> &[f32] {
	let lower_idx = (lower_freq / frequency_resolution) as usize;
	let upper_idx = (upper_freq / frequency_resolution) as usize;

	let sliced = &spectrum_data[lower_idx..=upper_idx];

	sliced
}

impl SignalParams {
	pub fn new(spectrum: &FrequencySpectrum) -> Self {
		let energy = spectrum.average().val();

		let data = spectrum
			.data()
			.into_iter()
			.map(|(freq, val)| val.val())
			.collect::<Vec<_>>();
		let len = data.len() as f32;

		let frequency_resolution = spectrum.frequency_resolution();
		let user1 = split_freqs(&data, frequency_resolution, 0.0, 100.0);
		let user1 = user1.iter().sum::<f32>() / len; // user1.len() as f32;
		let user2 = split_freqs(&data, frequency_resolution, 200.0, 800.0);
		let user2 = user2.iter().sum::<f32>() / len; // user2.len() as f32;
		let user3 = split_freqs(&data, frequency_resolution, 1000.0, 8000.0);
		let user3 = user3.iter().sum::<f32>() / len; // user3.len() as f32;
		let user4 = split_freqs(&data, frequency_resolution, 10000.0, 20000.0);
		let user4 = user4.iter().sum::<f32>() / len; // user4.len() as f32;

		Self {
			energy,
			user1,
			user2,
			user3,
			user4,
		}
	}

	pub(crate) fn energy(&self) -> f32 {
		self.energy
	}

	pub(crate) fn user1(&self) -> f32 {
		self.user1
	}

	pub(crate) fn user2(&self) -> f32 {
		self.user2
	}

	pub(crate) fn user3(&self) -> f32 {
		self.user3
	}

	pub(crate) fn user4(&self) -> f32 {
		self.user4
	}

	pub(crate) fn apply_ease(&mut self, ease: &impl crate::ease::Ease) {
		self.energy = ease.ease_in(self.energy);
		self.user1 = ease.ease_in(self.user1);
		self.user2 = ease.ease_in(self.user2);
		self.user3 = ease.ease_in(self.user3);
		self.user4 = ease.ease_in(self.user4);
	}
}
