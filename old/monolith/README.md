# Shader Art

Attempting to have some fun by following along with [this video][0] while learning [wgpu][1] with [this tutorial series][2]

[0]: https://youtu.be/f4s1h2YETNY
[1]: https://wgpu.rs/
[2]: https://sotrh.github.io/learn-wgpu/#what-is-wgpu
