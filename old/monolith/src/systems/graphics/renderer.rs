use std::collections::VecDeque;
use std::time::{Duration, Instant};

use tracing::{error, instrument, trace};
use tracing_unwrap::OptionExt;
use wgpu::util::DeviceExt;
use winit::dpi::{PhysicalPosition, PhysicalSize};
use winit::event::{ElementState, KeyboardInput, VirtualKeyCode, WindowEvent};
use winit::window::Window;

use super::super::audio::SignalParams;
use super::buffers::RawBuffer;
use super::{Buffers, Mesh, Vertex};
use crate::time::TimeTracker;

// TODO: Parameterize harder for modulating the strength value and all the
// places it's used/modified.
#[derive(Debug)]
pub(crate) struct Renderer {
	surface: wgpu::Surface,
	device: wgpu::Device,
	queue: wgpu::Queue,
	config: wgpu::SurfaceConfiguration,
	size: PhysicalSize<u32>,
	bg_color: wgpu::Color,
	render_pipeline: wgpu::RenderPipeline,
	signal_params: SignalParams,
}

impl Renderer {
	/// Creating some of the wgpu types requires async code
	///
	/// # Safety
	///
	/// Caller must ensure that the value returned from this function is dropped
	/// *before* the provided `window`. This requirement stems from the safety
	/// requirements of [`create_surface`].
	///
	/// [`create_surface`]: wgpu::Instance::create_surface
	pub(crate) async unsafe fn new(window: &Window) -> Self {
		let size = window.inner_size();
		if size.width == 0 || size.height == 0 {
			error!(size.width, size.height, "Window dims should not be 0");
		}

		let instance = Self::instance();

		// Safety: The surface needs to live at least as long as the window that
		// created it. The window is provided by the Engine, which we require to
		// enforce this property.
		let surface = unsafe { instance.create_surface(&window) }.unwrap();
		trace!(?surface, "got graphics surface");

		let adapter = Self::adapter(&instance, &surface).await;
		trace!(?adapter, "got graphics adapter");

		let (device, queue) = Self::device_queue(&adapter).await;
		trace!(?device, ?queue, "got graphics device");

		let surface_caps = surface.get_capabilities(&adapter);
		let surface_format = Self::surface_format(&surface_caps);
		let config = Self::config(size, &surface_caps, surface_format);
		surface.configure(&device, &config);

		let shader = Self::shader(&device);
		let render_pipeline = Self::render_pipeline(&device, &shader, &config);

		Self {
			surface,
			device,
			queue,
			config,
			size,
			bg_color: wgpu::Color {
				r: 0.1,
				g: 0.2,
				b: 0.3,
				a: 1.0,
			},
			render_pipeline,
			signal_params: Default::default(),
		}
	}

	pub fn device(&self) -> &wgpu::Device {
		&self.device
	}
	pub fn size(&self) -> PhysicalSize<u32> {
		self.size
	}
	pub fn background(&self) -> wgpu::Color {
		self.bg_color
	}

	pub fn set_background(&mut self, background: wgpu::Color) {
		self.bg_color = background;
	}

	// Hey Ky,
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	// I know messing around with this is fun and all, but I also would
	// like to see this project get to an even more cool place by properly
	// implementing portions of the audio pipeline to take into account frame
	// timing vs audio timing and how those interactions take place, and it may
	// involve a bit of convolution so check out 3blue1brown on youtube for
	// their convolution content!
	pub(crate) fn set_signal_params(&mut self, signal_params: SignalParams) {
		self.signal_params = signal_params;
	}

	pub(crate) fn resize(&mut self, new_size: PhysicalSize<u32>) {
		trace!(new_size.width, new_size.height, "Setting new window size:");
		if new_size.width > 0 && new_size.height > 0 {
			self.size = new_size;
			self.config.width = new_size.width;
			self.config.height = new_size.height;
			self.surface.configure(&self.device, &self.config);
		} else {
			error!(
				new_size.width,
				new_size.height, "Tried to set window size where at least one dimension was 0"
			);
		}
	}

	// TODO: Make this a callback that is handled by an input system. Probably
	// make it a separate object/system that modifies the renderer rather it
	// being the renderer modifying itself?
	pub(crate) fn input(&mut self, event: &WindowEvent) -> bool {
		match event {
			WindowEvent::CursorMoved { position, .. } => self.handle_cursor_moved(*position),
			// WindowEvent::KeyboardInput {
			//	input:
			//		KeyboardInput {
			//			state,
			//			virtual_keycode: Some(keycode),
			//			..
			//		},
			//	..
			// } => self.handle_keyboard(*state, *keycode),
			_ => false,
		}
	}

	// TODO: Do we want to keep this around. It's not used now but I'm not sure
	// if there may be a reason in the future for it to be used.
	pub(crate) fn update(&mut self, dt: f32) {}

	pub(crate) fn render<'m>(
		&mut self,
		meshes: impl Iterator<Item = &'m Mesh>,
	) -> Result<(), wgpu::SurfaceError> {
		let output = self.output()?;
		let view = Self::view(&output);
		let mut encoder = self.encoder();
		let mut render_pass = self.render_pass(&mut encoder, &view);

		// Set the pipeline on `render_pass` to what we created in
		// `Renderer::new`
		render_pass.set_pipeline(&self.render_pipeline);

		for mesh in meshes {
			mesh.render(&mut render_pass);
		}
		// Need to drop the mutable borrow of `encoder` before we can reuse it
		drop(render_pass);

		// Submit will accept anything that implements IntoIter
		self.queue.submit(std::iter::once(encoder.finish()));
		output.present();

		Ok(())
	}

	fn instance() -> wgpu::Instance {
		// The instance is a handle to our GPU
		// Backends::all => Vulkan + Metal + DX12 + Browser WebGPU
		wgpu::Instance::new(
			/*wgpu::InstanceDescriptor {
				backends: wgpu::Backends::all(),
				dx12_shader_compiler: Default::default(),
			}*/
			Default::default(),
		)
	}
	async fn adapter(instance: &wgpu::Instance, surface: &wgpu::Surface) -> wgpu::Adapter {
		/*instance
		.request_adapter(&wgpu::RequestAdapterOptions {
			power_preference: wgpu::PowerPreference::LowPower,
			compatible_surface: None,//Some(&surface),
			force_fallback_adapter: false,
		})
		.await
		.expect_or_log("wgpu couldn't find a suitable GPU to use")*/
		instance
			// .enumerate_adapters(wgpu::Backends::VULKAN)
			.enumerate_adapters(wgpu::Backends::PRIMARY)
			.next()
			.expect_or_log("No supported GPUs found")
	}
	async fn device_queue(adapter: &wgpu::Adapter) -> (wgpu::Device, wgpu::Queue) {
		adapter
			.request_device(
				&wgpu::DeviceDescriptor {
					features: wgpu::Features::empty(),
					limits: wgpu::Limits::default(),
					label: None,
				},
				None, // Trace path
			)
			.await
			.unwrap()
	}
	fn surface_format(surface_caps: &wgpu::SurfaceCapabilities) -> wgpu::TextureFormat {
		let formats = surface_caps.formats.as_slice();
		if formats.is_empty() {
			error!("The surface doesn't have any supported texture formats");
			panic!();
		}
		formats
			.iter()
			.copied()
			.find(|f| !f.is_srgb())
			.unwrap_or(surface_caps.formats[0])
	}
	fn config(
		size: PhysicalSize<u32>,
		surface_caps: &wgpu::SurfaceCapabilities,
		surface_format: wgpu::TextureFormat,
	) -> wgpu::SurfaceConfiguration {
		wgpu::SurfaceConfiguration {
			usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
			format: surface_format,
			width: size.width,
			height: size.height,
			// present_mode: wgpu::PresentMode::AutoNoVsync,
			present_mode: wgpu::PresentMode::AutoVsync,
			alpha_mode: surface_caps.alpha_modes[0],
			view_formats: vec![],
		}
	}
	fn shader(device: &wgpu::Device) -> wgpu::ShaderModule {
		device.create_shader_module(wgpu::ShaderModuleDescriptor {
			label: Some("Shader"),
			source: wgpu::ShaderSource::Wgsl(include_str!("shader.wgsl").into()),
		})
	}
	// TODO: Break this up into smaller chunks, probably via a wrapping struct
	// of some kind.
	fn render_pipeline(
		device: &wgpu::Device,
		shader: &wgpu::ShaderModule,
		config: &wgpu::SurfaceConfiguration,
	) -> wgpu::RenderPipeline {
		let render_pipeline_layout =
			device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
				label: Some("Render Pipeline Layout"),
				bind_group_layouts: &[],
				push_constant_ranges: &[],
			});

		device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
			label: Some("Render Pipeline"),
			layout: Some(&render_pipeline_layout),
			vertex: wgpu::VertexState {
				module: shader,
				// Specifies the function in the shader that is the entry
				// point for the vertex shader.
				entry_point: "vs_main",
				// Tells `wgpu` what type of vertices we want to pass to the
				// vertex shader. We're specifying the vertices in the
				// shader itself, so it's left empty.
				buffers: &[Vertex::desc()],
			},
			fragment: Some(wgpu::FragmentState {
				// Fragment shader is technically optional, so it's wrapped
				// in `Some`. It's needed to store color data to the
				// surface.
				module: shader,
				entry_point: "fs_main",
				targets: &[Some(wgpu::ColorTargetState {
					// Tells `wgpu` what color outputs it should set up.
					// Currently only one is needed for the surface. The
					// surface's format is used to make copying to it easy,
					// and specifying blending to just replace old pixel
					// data with new data. Also specifies to write all of
					// red, green, blue, and alpha.
					format: config.format,
					blend: Some(wgpu::BlendState::REPLACE),
					write_mask: wgpu::ColorWrites::ALL,
				})],
			}),
			// Describes how to interpret the vertices when converting them
			// to triangles
			primitive: wgpu::PrimitiveState {
				// Using `TriangleList` means that every three vertices
				// correspond to one triangle.
				topology: wgpu::PrimitiveTopology::TriangleList,
				strip_index_format: None,
				// `front_face` and `cull_mode` tell `wgpu` how to determine
				// whether a given triangle is facing forward or not.
				// `FrontFace::Ccw` means that a triangle is facing forward
				// if the vertices are arranged in a counter-clockwise
				// direction. Triangles are not considered facing forward
				// are culled as specified by `CullMode::Back`.
				front_face: wgpu::FrontFace::Ccw,
				cull_mode: Some(wgpu::Face::Back),
				// Setting this to anything other than `Fill` requires
				// `Features::NON_FILL_POLYGON_MODE`
				polygon_mode: wgpu::PolygonMode::Fill,
				// Requires `Features::DEPTH_CLIP_CONTROL`
				unclipped_depth: false,
				// Requires `Features::CONSERVATIVE_RASTERIZATION`
				conservative: false,
			},
			// Depth/stencil buffer is not used currently. *This will change
			// later*.
			depth_stencil: None,
			multisample: wgpu::MultisampleState {
				// `count` determines how many samples the pipeline will
				// use. Multisampling is complex and not appropriate for a
				// beginner's tutorial.
				// count: 4,
				count: 1,
				// `mask` specifies which samples should be active, in this
				// case all of them.
				mask: !0,
				// `alpha_to_coverage_enabled` has to do with anti-aliasing,
				// which is not covered in this tutorial.
				alpha_to_coverage_enabled: false,
			},
			// `multiview` indicates how many array layers the render
			// attachments can have.
			multiview: None,
		})
	}

	// TODO: See `Self::input`.
	fn handle_cursor_moved(&mut self, position: winit::dpi::PhysicalPosition<f64>) -> bool {
		let polar = crate::utils::cursor_pos_polar(position, self.size);
		let hsv = crate::utils::Hsv::new(
			polar.theta() / (std::f32::consts::PI * 2.) + 0.5,
			1.0,
			polar.radius(),
		);
		let color = hsv.to_rgba(1.);
		self.bg_color = crate::utils::rgba(color);
		true
	}
	// fn handle_keyboard(
	//	&mut self,
	//	state: ElementState,
	//	keycode: VirtualKeyCode,
	// ) -> bool {
	//	match keycode {
	//		VirtualKeyCode::Space => {
	//			if matches!(state, ElementState::Released) {
	//				self.space_is_pressed = false;
	//			} else {
	//				self.space_is_pressed = true;
	//			}
	//			true
	//		}
	//		_ => false,
	//	}
	// }

	/// The frame to render in
	fn output(&self) -> Result<wgpu::SurfaceTexture, wgpu::SurfaceError> {
		self.surface.get_current_texture()
	}
	/// Create a texture view to control how the render code interacts with
	/// the texture
	fn view(output: &wgpu::SurfaceTexture) -> wgpu::TextureView {
		output.texture.create_view(&wgpu::TextureViewDescriptor::default())
	}
	/// Create a command encoder to send commands to the GPU
	fn encoder(&self) -> wgpu::CommandEncoder {
		self.device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
			label: Some("Render Encoder"),
		})
	}
	fn render_pass<'me, 'encoder, 'view>(
		&'me self,
		encoder: &'encoder mut wgpu::CommandEncoder,
		view: &'view wgpu::TextureView,
	) -> wgpu::RenderPass
	where
		'encoder: 'me,
		'view: 'me,
	{
		// TODO: Break this out into a separate module/crate
		fn min_limit(input: f64, limit: f64) -> f64 {
			comparer(input, limit) * input
		}
		fn comparer(input: f64, limit: f64) -> f64 {
			if input < limit {
				0.0
			} else {
				1.0
			}
		}
		encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
			label: Some("Render Pass"),
			color_attachments: &[Some(wgpu::RenderPassColorAttachment {
				view,
				resolve_target: None,
				ops: wgpu::Operations {
					load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
					store: true,
				},
			})],
			depth_stencil_attachment: None,
		})
	}
}
