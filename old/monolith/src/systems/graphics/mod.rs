pub(crate) use buffers::Buffers;
pub(crate) use mesh::Mesh;
pub(crate) use renderer::Renderer;
pub(crate) use vertex::Vertex;

pub(crate) mod buffers;
pub(crate) mod mesh;
pub(crate) mod renderer;
pub(crate) mod shader_params;
pub(crate) mod vertex;

type Vec2<T> = [T; 2];
type Vec3<T> = [T; 3];
