use std::fmt::Debug;

use crate::systems::graphics::Mesh;

pub(crate) trait Object: Debug {
	fn objectify(self) -> Box<dyn Object>
	where
		Self: Sized + 'static,
	{
		Box::new(self)
	}

	fn mesh(&self) -> Option<&Mesh> {
		None
	}
	fn mesh_mut(&mut self) -> Option<&mut Mesh> {
		None
	}

	/// Update internal state
	///
	/// # Meshes
	///
	/// If the object contains a mesh, [`Object::mesh`] and [`Object::mesh_mut`]
	/// *must* be overridden. Without overriding those methods, the mesh will
	/// never be drawn, and can't be updated. Additionally, [`Mesh::update`]
	/// takes a [`wgpu::Device`] reference as a parameter, which can only
	/// be provided by the [`Engine`](crate::Engine), so without overriding
	/// those methods, the meshes won't get updated.
	fn update(&mut self, dt: f32);
}

impl Object for Box<dyn Object> {
	fn objectify(self) -> Box<dyn Object>
	where
		Self: Sized,
	{
		self
	}

	fn update(&mut self, dt: f32) {
		self.as_mut().update(dt);
	}

	fn mesh(&self) -> Option<&Mesh> {
		self.as_ref().mesh()
	}

	fn mesh_mut(&mut self) -> Option<&mut Mesh> {
		self.as_mut().mesh_mut()
	}
}
