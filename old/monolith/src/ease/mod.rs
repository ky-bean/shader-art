pub trait Ease {
	fn ease_in(&self, input: f32) -> f32;
	fn ease_out(&self, input: f32) -> f32;
	fn ease_in_out(&self, input: f32) -> f32;
}

enum Bound<T> {
	Lower(T),
	Upper(T),
	Both { lower: T, upper: T },
}
impl<T: PartialOrd> Bound<T> {
	fn assert_bound(&self) -> bool {
		match self {
			Bound::Lower(_) | Bound::Upper(_) => true,
			Bound::Both { lower, upper } => lower < upper,
		}
	}
}
struct BoundedParam<T, F> {
	value: T,
	assert: F,
}
impl<T: Copy, F: Fn(T) -> bool> BoundedParam<T, F> {
	fn new(value: T, assert: F) -> Option<Self> {
		if !assert(value) {
			return None;
		}

		Some(Self { value, assert })
	}
	fn set_value(&mut self, value: T) {
		if (self.assert)(value) {
			self.value = value;
		}
	}
	fn value(&self) -> T {
		self.value
	}
}

macro_rules! impl_ease {
    ($struct_vis:vis struct $name:ident {$($vis:vis $field:ident: $ty:ty),*$(,)?} $base_fn:item $base_ident:ident) => {
        $struct_vis struct $name {
            $($vis $field: $ty),*
        }
        impl $name {
            $base_fn
            fn __base(&self, input: f32) -> f32 {
                if input <= 0.0 { return 0.0; }
                if input >= 1.0 { return 1.0; }

                self.$base_ident(input)
            }
        }
        impl $crate::ease::Ease for $name {
            fn ease_in(&self, input: f32) -> f32 {
                self.__base(input)
            }
            fn ease_out(&self, input: f32) -> f32 {
                1.0 - self.__base(1.0 - input)
            }
            fn ease_in_out(&self, input: f32) -> f32 {
                if input < 0.5 {
                    self.ease_in(2.0*input) / 2.0
                } else {
                    (self.ease_out(2.0*input - 1.0) + 1.0) / 2.0
                }
            }
        }
    };
}

// TODO: Impl Default for all of these

impl_ease! {
	pub struct Sine {}
	fn base_impl(&self, input: f32) -> f32 {
		1.0 - (input * std::f32::consts::FRAC_PI_2).cos()
	}
	base_impl
}
impl_ease! {
	pub struct Quadratic{pub power: i32}
	fn base_impl(&self, input: f32) -> f32 {
		input.powi(self.power)
	}
	base_impl
}
impl_ease! {
	pub struct Exponential{pub base: f32, pub power: f32}
	fn base_impl(&self, input: f32) -> f32 {
		if input == 0.0 {
			return 0.0;
		}

		self.base.powf(self.power*(input - 1.0))
	}
	base_impl
}
impl_ease! {
	pub struct Circle{}
	fn base_impl(&self, input: f32) -> f32 {
		1.0 - (1.0 - input*input).sqrt()
	}
	base_impl
}
impl_ease! {
	pub struct Back{pub strength: f32}
	fn base_impl(&self, input: f32) -> f32 {
		let c = self.strength;

		(c + 1.0) * input*input*input - c * input*input
	}
	base_impl
}
impl_ease! {
	pub struct Elastic { pub strength: f32 }
	fn base_impl(&self, input: f32) -> f32 {
		todo!()
	}
	base_impl
}
type AssertLogorithmic = fn(f32) -> bool;
impl_ease! {
	pub struct Logarithmic { base: BoundedParam<f32, AssertLogorithmic>}
	fn base_impl(&self, input: f32) -> f32 {
		let base = self.base.value();

		if base == 1.0 {
			return input;
		}

		let log_param = (base - 1.0 / base) * input + 1.0 / base;
		let log = log_param.log(base);

		(log + 1.0) / 2.0
	}
	base_impl
}
impl Logarithmic {
	pub fn new(base: f32) -> Option<Self> {
		let assert: AssertLogorithmic = Self::assert_base;
		BoundedParam::new(base, assert).map(|param| Self { base: param })
	}

	fn assert_base(base: f32) -> bool {
		base >= 1.0
	}

	fn base(&self) -> f32 {
		self.base.value()
	}
	fn set_base(&mut self, base: f32) {
		self.base.set_value(base);
	}
}
