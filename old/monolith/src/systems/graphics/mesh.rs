use wgpu::util::DeviceExt;

use super::buffers::RawBuffer;
use super::shader_params::ShaderParams;
use crate::systems::audio::SignalParams;

#[derive(Debug)]
pub(crate) struct Mesh {
	buf: RawBuffer,
	vertices: wgpu::Buffer,
	indices: wgpu::Buffer,
}

impl Mesh {
	pub fn new(buf: RawBuffer, device: &wgpu::Device) -> Self {
		let vertices = Self::vertices_buffer(&buf, device);
		let indices = Self::indices_buffer(&buf, device);
		Self {
			buf,
			vertices,
			indices,
		}
	}

	pub fn update(
		&mut self,
		shader_params: ShaderParams,
		device: &wgpu::Device,
	) {
		for vertex in self.buf.vertices.iter_mut() {
			vertex.update(shader_params);
		}

		// FIXME: It feels wrong to need to generate a new buffer every frame,
		// but I can't figure out a way to modify the contents of the buffer
		// in-place in order to modify the vertices. I've tried messing with
		// `wgpu::Buffer::slice(_, ..).get_mapped_range_mut(_)` but I don't
		// understand how the buffer usage flags work in a way to get wgpu to
		// let me modify it. It seems like maybe vertex buffers don't support
		// that mapping operation and I can't find any other method that would
		// get me access to the underlying buffer.
		self.vertices = Self::vertices_buffer(&self.buf, &device);
	}

	pub fn notify_resize(&mut self, viewport_dims: [u32; 2]) {
		for vertex in self.buf.vertices.iter_mut() {
			vertex.update_viewport_dims(viewport_dims);
		}
	}

	fn num_indices(&self) -> u32 {
		self.buf.indices.len() as u32
	}

	pub(crate) fn render<'me, 'r>(
		&'me self,
		render_pass: &mut wgpu::RenderPass<'r>,
	) where
		'me: 'r,
	{
		let num_indices = self.num_indices();
		// exit early if there's nothing to render
		if num_indices == 0 {
			return;
		}

		render_pass.set_vertex_buffer(0, self.vertices.slice(..));
		render_pass.set_index_buffer(
			self.indices.slice(..),
			wgpu::IndexFormat::Uint16,
		);
		render_pass.draw_indexed(0..self.num_indices(), 0, 0..1);
	}

	fn vertices_buffer(buf: &RawBuffer, device: &wgpu::Device) -> wgpu::Buffer {
		device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
			label: Some("Vertex Buffer"),
			contents: bytemuck::cast_slice(&buf.vertices),
			usage: wgpu::BufferUsages::VERTEX,
		})
	}
	fn indices_buffer(buf: &RawBuffer, device: &wgpu::Device) -> wgpu::Buffer {
		device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
			label: Some("Index Buffer"),
			contents: bytemuck::cast_slice(&buf.indices),
			usage: wgpu::BufferUsages::INDEX,
		})
	}
}
