use std::sync::mpsc;
use std::time::Duration;

use cpal::StreamInstant;
use tracing::{debug, error, trace};
use tracing_unwrap::OptionExt;

use super::super::{Sample, SamplesBuf};

pub(in crate::systems::audio) type InputPayload = (StreamInstant, SamplesBuf);

#[derive(Debug)]
pub(in crate::systems::audio) struct CpalStreamHandler {
	sender: mpsc::Sender<InputPayload>,
	start_time: Option<StreamInstant>,
	current_time: Option<StreamInstant>,
}
impl CpalStreamHandler {
	pub(super) fn new(sender: mpsc::Sender<InputPayload>) -> Self {
		Self {
			sender,
			start_time: None,
			current_time: None,
		}
	}
	#[tracing::instrument(skip(self, samples, info), fields(timestamp))]
	pub(super) fn process_input(
		&mut self,
		samples: &[Sample],
		info: &cpal::InputCallbackInfo,
	) {
		self.update_time(info.timestamp().capture);
		self.report_timestamp();
		self.send_samples(samples);
	}
	fn report_timestamp(&mut self) -> Option<Duration> {
		match (self.start_time, self.current_time) {
			(Some(start_time), Some(current_time)) => {
				super::report_timestamp("timestamp", start_time, current_time)
			}
			_ => None,
		}
	}
	fn update_time(&mut self, now: StreamInstant) {
		// Sometimes monotonicity is a lie, and it can be good to make sure that
		// start_time truly is lower than all other instants.
		//
		// I've encountered an issue of the instants not being monotonic at the
		// very start of a stream after being initialized, so presumably there's
		// some synchronization issue between when the input thread callback
		// loop is started and when the ADC is ready to provide valid data.
		if let Some(start_time) = self.start_time {
			// If now is in the past, update the past
			if now < start_time {
				error!(
					new_time = ?now,
					old_time = ?start_time,
					time_change = start_time.duration_since(&now).map(|dur| dur.as_secs_f32()),
					"Time went backwards!"
				);
				self.start_time = Some(now);
				return;
			}

			// Set the current time
			self.current_time = Some(now);
		} else {
			self.start_time = Some(now);
		}
	}
	fn send_samples(&self, samples: &[Sample]) {
		if let Some(current_time) = self.current_time {
			trace!(
				"Sending {} samples to the spectrum processor",
				samples.len()
			);
			if self
				.sender
				.send((current_time, SamplesBuf::from(samples)))
				.is_err()
			{
				error!("The spectrum processor hung up, this thread should be stopped soon (hopefully the next frame at the latest)...");
			}
		}
	}
}
#[derive(Debug)]
pub(in crate::systems::audio) struct CpalStreamErrorHandler {}
impl CpalStreamErrorHandler {
	pub(super) fn new() -> Self {
		Self {}
	}
	#[tracing::instrument(skip(self, err))]
	pub(super) fn process_error(&mut self, err: cpal::StreamError) {
		error!(
			?err,
			"Encountered an error while processing audio from the system."
		);
	}
}
