use wgpu::Device;

// TODO: Move this trait out here and the manager into its own module.
use self::object::Object;
use super::audio::SignalParams;
use super::graphics::shader_params::ShaderParams;
use super::graphics::{Mesh, Renderer};

pub(crate) mod object;
pub(crate) mod simple;

#[derive(Debug)]
pub(crate) struct ObjectSystem {
	objects: Vec<Box<dyn Object>>,
}

impl ObjectSystem {
	pub fn new() -> Self {
		Self {
			objects: Vec::new(),
		}
	}

	pub fn add_object(&mut self, object: impl Object + 'static) {
		self.objects.push(object.objectify());
	}

	pub fn update(&mut self, dt: f32) {
		for object in self.objects.iter_mut() {
			object.update(dt);
		}
	}
	pub fn update_meshes(
		&mut self,
		shader_params: ShaderParams,
		device: &Device,
	) {
		for mesh in self.meshes_mut() {
			mesh.update(shader_params, device);
		}
	}

	pub fn notify_resize(&mut self, viewport_dims: [u32; 2]) {
		for mesh in self.meshes_mut() {
			mesh.notify_resize(viewport_dims);
		}
	}

	pub fn render_meshes(
		&self,
		renderer: &mut Renderer,
	) -> Result<(), wgpu::SurfaceError> {
		renderer.render(self.meshes())
	}

	fn meshes(&self) -> impl Iterator<Item = &'_ Mesh> {
		self.objects.iter().flat_map(|obj| obj.mesh())
	}
	fn meshes_mut(&mut self) -> impl Iterator<Item = &'_ mut Mesh> {
		self.objects.iter_mut().flat_map(|obj| obj.mesh_mut())
	}
}
