use spectrum_analyzer::{Frequency, FrequencySpectrum, FrequencyValue};
use systems::audio::AudioSystem;
use systems::graphics::{self, Mesh, Renderer, Vertex};
use systems::objects::object::Object;
use systems::objects::simple::Simple;
use systems::objects::ObjectSystem as ObjectManager;
use tracing::{debug, error, info, trace};
use tracing_unwrap::{OptionExt, ResultExt};
use winit::dpi::PhysicalSize;
use winit::event::{
	DeviceEvent, DeviceId, ElementState, Event, KeyboardInput, VirtualKeyCode,
	WindowEvent,
};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::{Fullscreen, Window, WindowBuilder, WindowId};

use crate::systems;
use crate::systems::graphics::shader_params::ShaderParams;
use crate::time::{TimePoint, TimeTracker};

type CustomEvent = ();
// #[derive(Debug)]
pub(crate) struct Engine {
	audio_system: AudioSystem,
	object_manager: ObjectManager,
	renderer: Renderer,
	time_tracker: TimeTracker,

	// Safety: This value MUST be placed *after* `renderer` in the struct
	// definition, see `Self::new`.
	window: Window,
}
impl Engine {
	#[tracing::instrument]
	pub async fn new(window: Window) -> Self {
		let audio_system = AudioSystem::new();

		window.set_min_inner_size(Some(PhysicalSize::new(256, 256)));
		// window.set_max_inner_size(Some(PhysicalSize::new(960, 960)));

		// Create renderer

		// Safety: The struct definition places `window` after `renderer`, which
		// the compiler enforces to be the order in which the values are
		// dropped.
		let renderer = unsafe { Renderer::new(&window) }.await;

		let mut object_manager = ObjectManager::new();

		let size = window.inner_size();
		let square = Mesh::new(
			Vertex::square([size.width, size.height]),
			renderer.device(),
		);

		object_manager.add_object(Simple::new(square));

		Self {
			audio_system,
			object_manager,
			window,
			renderer,
			time_tracker: TimeTracker::new(),
		}
	}

	#[tracing::instrument(skip(self, event, control_flow))]
	pub(crate) fn handle_winit_event(
		&mut self,
		event: Event<'_, CustomEvent>,
		control_flow: &mut ControlFlow,
	) {
		match event {
			Event::NewEvents(cause) => {
				// Emitted when new events arrive from the OS to be processed.
				// Used for code that should be run before processing events,
				// such as updating frame timing.
				trace!("Recieved NewEvents({cause:?})",);
				self.on_loop_start();
			}
			Event::WindowEvent {
				window_id,
				ref event,
			} => {
				// Emitted when the OS sends an event to a winit window.
				trace!("Recieved WindowEvent{{{window_id:?}, {event:?}}}");
				self.on_window_event(window_id, event, control_flow);
			}
			Event::DeviceEvent { device_id, event } => {
				// Emitted when the OS sends an event to a device.
				trace!("Recieved DeviceEvent{{{device_id:?}, {event:?}}}");
				self.on_device_event(device_id, event);
			}
			Event::UserEvent(event) => {
				// Emitted when an event is sent from
				// `EventLoopProxy::send_event`.
				trace!("Recieved UserEvent({event:?})");
				self.on_user_event(event);
			}
			Event::Suspended => {
				// Emitted when the application has been suspended.
				trace!("Recieved Suspended");
				self.on_suspended();
			}
			Event::Resumed => {
				// Emitted when the application has been resumed from
				// suspension, as well as when the program starts, i.e. after
				// the `NewEvents(StartCause::Init)` event.
				trace!("Recieved Resumed");
				self.on_resumed();
			}
			Event::MainEventsCleared => {
				// Emitted when all of the event loop's input events have been
				// processed and redraw processing is about to begin. This is
				// for all code that should be run after state-changing events
				// have been handled and do "Main Loop" type operations.
				trace!("Recieved MainEventsCleared");
				self.on_main_events_cleared();
			}
			Event::RedrawRequested(window_id) => {
				// Emitted after `MainEventsCleared` when a window should be
				// redrawn.
				trace!("Recieved RedrawRequested({window_id:?})");
				self.on_redraw_requested(window_id, control_flow);
			}
			Event::RedrawEventsCleared => {
				trace!("Recieved RedrawEventsCleared");
				self.on_redraw_events_cleared();
			}
			Event::LoopDestroyed => {
				trace!("Recieved LoopDestroyed");
				self.on_loop_destroyed();
			}
		}
	}

	#[tracing::instrument(skip(self))]
	fn on_loop_start(&mut self) {
		let TimePoint { dt, elapsed } = self.time_tracker.update();
	}

	#[tracing::instrument(skip(self, control_flow))]
	pub fn on_window_event(
		&mut self,
		window_id: WindowId,
		event: &WindowEvent,
		control_flow: &mut ControlFlow,
	) {
		fn flatten_size(size: PhysicalSize<u32>) -> [u32; 2] {
			[size.width, size.height]
		}
		if window_id == self.window.id() && !self.renderer.input(event) {
			match event {
				// TODO: Break this out into an input system
				WindowEvent::CloseRequested => {
					*control_flow = ControlFlow::Exit
				}
				WindowEvent::KeyboardInput { input, .. } => {
					if input.state == ElementState::Pressed
						&& input.virtual_keycode == Some(VirtualKeyCode::Escape)
					{
						*control_flow = ControlFlow::Exit;
						return;
					}

					if input.state == ElementState::Pressed
						&& input.virtual_keycode == Some(VirtualKeyCode::F)
					{
						let monitor_handle =
							self.window.current_monitor().unwrap_or_log();
						let is_fullscreen = self.window.fullscreen().is_some();
						if is_fullscreen {
							self.window.set_fullscreen(None);
						} else {
							let fullscreen =
								Fullscreen::Borderless(Some(monitor_handle));
							self.window.set_fullscreen(Some(fullscreen));
						}
					}
				}
				WindowEvent::Resized(physical_size) => {
					self.renderer.resize(*physical_size);
					self.object_manager
						.notify_resize(flatten_size(*physical_size));
				}
				WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
					self.renderer.resize(**new_inner_size);
					self.object_manager
						.notify_resize(flatten_size(**new_inner_size));
				}
				_ => {}
			}
		}
	}

	#[tracing::instrument(skip(self))]
	fn on_device_event(&mut self, device_id: DeviceId, event: DeviceEvent) {}

	#[tracing::instrument(skip(self))]
	fn on_user_event(&mut self, event: CustomEvent) {}

	#[tracing::instrument(skip(self))]
	fn on_suspended(&mut self) {}

	#[tracing::instrument(skip(self))]
	fn on_resumed(&mut self) {}

	#[tracing::instrument(skip(self))]
	fn on_main_events_cleared(&mut self) {
		let time = self.time_tracker.time_point();
		let dt = self.time_tracker.dt();

		self.audio_system.update();
		let mut signal_params = self.audio_system.signal_params();
		let energy = signal_params.energy();
		trace!(energy);
		self.renderer.set_signal_params(signal_params.clone());

		self.object_manager.update(dt);

		use crate::ease::Ease;
		let ease = crate::ease::Logarithmic::new(2.0)
			.expect("Used invalid base for logarthmic ease!");
		signal_params.apply_ease(&ease);
		let shader_params = ShaderParams::new(time, signal_params);
		self.object_manager
			.update_meshes(shader_params, &self.renderer.device());

		// TODO: Make abstraction over systems that encompass the renderer, time
		// tracker, audio processor, etc.
		self.renderer.update(dt);

		// Tell winit that the window should be redrawn
		self.window.request_redraw();
	}

	#[tracing::instrument(skip(self, control_flow))]
	fn on_redraw_requested(
		&mut self,
		window_id: WindowId,
		control_flow: &mut ControlFlow,
	) {
		if window_id != self.window.id() {
			return;
		}

		match self.object_manager.render_meshes(&mut self.renderer) {
			Ok(_) => {}
			// Reconfigure the surface if lost
			Err(wgpu::SurfaceError::Lost) => {
				self.renderer.resize(self.renderer.size())
			}
			// The system is out of memory, we should probably quit
			Err(wgpu::SurfaceError::OutOfMemory) => {
				error!("System ran out of memory, exiting...");
				*control_flow = ControlFlow::Exit
			}
			// All other errors (Outdated, Timeout) should be resolved by
			// the next frame
			Err(e) => error!(?e),
		}
	}

	#[tracing::instrument(skip(self))]
	fn on_redraw_events_cleared(&mut self) {}

	#[tracing::instrument(skip(self))]
	fn on_loop_destroyed(&mut self) {
		info!("Exiting main loop");
	}
}
