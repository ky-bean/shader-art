#[cfg(target_os = "windows")]
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};

fn main() {
	#[cfg(target_os = "windows")]
	{
		let host = cpal::default_host();
		let wasapi_host = match host.as_inner() {
			cpal::platform::HostInner::Wasapi(host) => host,
		};
		println!("Host: {:?}", wasapi_host);
		let device = host.default_input_device().unwrap();
		println!("Device: {}", device.name().unwrap_or_default());
		let config = device.default_input_config().unwrap();
		let stream = device
			.build_input_stream(
				&config.config(),
				move |data: &[f32], _| println!("Received {} samples", data.len()),
				move |err| eprintln!("{err}"),
				None,
			)
			.unwrap();
		stream.play().unwrap();
		std::thread::sleep(std::time::Duration::from_secs(60));
	}
}
