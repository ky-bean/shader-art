use std::collections::VecDeque;
use std::sync::mpsc::RecvTimeoutError;
use std::sync::{mpsc, Arc};
use std::time::Duration;

use cpal::StreamInstant;
use spectrum_analyzer::error::SpectrumAnalyzerError;
use spectrum_analyzer::scaling::{SpectrumDataStats, SpectrumScalingFunction};
use spectrum_analyzer::FrequencySpectrum;
use tracing::{debug, error, field, info, trace};

use crate::systems::audio::input::cpal_handler::InputPayload;
use crate::systems::audio::{Sample, SamplesBuf};

type ScalingFn = Box<dyn Fn(f32, &SpectrumDataStats) -> f32 + 'static + Send>;

const SPECTRUM_WIDTH: usize = 0x4000;

pub(in crate::systems::audio) struct SpectrumProcessor {
	receiver: mpsc::Receiver<InputPayload>,
	sender: mpsc::Sender<FrequencySpectrum>,

	sample_buffer: VecDeque<Sample>,

	sampling_rate: u32,
	scaling_fn: Option<ScalingFn>,

	start_time: Option<StreamInstant>,

	retries: u8,
}
impl SpectrumProcessor {
	/// Create a new spectrum processor.
	///
	/// `desired_resolution` is used to specify a desired spectral resolution of
	/// the
	pub fn new(
		receiver: mpsc::Receiver<InputPayload>,
		sender: mpsc::Sender<FrequencySpectrum>,
		sampling_rate: u32,
		scaling_fn: Option<ScalingFn>,
	) -> Self {
		Self {
			receiver,
			sender,
			sample_buffer: VecDeque::with_capacity(2 * SPECTRUM_WIDTH),
			sampling_rate,
			scaling_fn,
			start_time: None,
			retries: 8,
		}
	}

	// TODO: Gotta figure out a more ergonomic way to structure/organize the
	// functions in this call tree.
	// TODO: ^^^^^^^^ **Definitely** need to do this ^^^^^^^^
	#[tracing::instrument(
		name = "SpectrumProcessor:ThreadLoop",
		skip(self),
		fields(timestamp = field::Empty)
	)]
	pub fn processor_thread(mut self) {
		info!("Spectrum processing thread started successfully");
		loop {
			if self.process().is_err() {
				break;
			}
			// Every call to `self.process()` does as much work as it can, so
			// there's nothing left to do and spinning in this loop isn't going
			// to do much for us.
			std::thread::yield_now();
		}
		info!("Spectrum processing thread is exiting");
	}

	// FIXME: Use a meaningful type at least for the error type.
	fn process(&mut self) -> Result<(), ()> {
		let timeout = Duration::from_secs_f32(0.25);
		trace!(
			timeout = timeout.as_secs_f32(),
			"Waiting for samples from the input thread"
		);
		match self.receiver.recv_timeout(timeout) {
			Ok(msg) => self.process_message(msg),
			Err(RecvTimeoutError::Timeout) => {
				self.retries -= 1;
				error!(
					?timeout,
					retries_remaining = self.retries,
					"Timed out waiting to receive samples from the input"
				);
				if self.retries != 0 {
					Ok(())
				} else {
					error!("Exceeded retries count, exiting thread...");
					Err(())
				}
			}
			Err(_) => {
				info!("Audio input thread hung up, exiting thread...");
				Err(())
			}
		}
	}

	fn process_message(
		&mut self,
		(timestamp, samples): InputPayload,
	) -> Result<(), ()> {
		self.update_time(timestamp);
		trace!("Recieved {} samples from input thread", samples.len());

		let spectra = self.process_samples(samples);

		trace!(
			"Sending {} spectrum frames to the main thread",
			spectra.len()
		);
		for spectrum in spectra {
			if self.sender.send(spectrum).is_err() {
				info!("main thread hung up, exiting...");
				return Err(());
			}
		}
		Ok(())
	}

	fn process_samples(
		&mut self,
		samples: SamplesBuf,
	) -> Vec<FrequencySpectrum> {
		trace!("Appending {} samples to buffer", samples.len());
		self.sample_buffer.extend(&samples);

		let mut spectra =
			Vec::with_capacity(self.sample_buffer.len() / SPECTRUM_WIDTH);

		// TODO: Instead of draining the buffer 16384 samples at a time use a
		// moving window over samples. This should help with not using such a
		// high sample rate in the input thread.
		loop {
			if self.sample_buffer.len() > SPECTRUM_WIDTH {
				let spectrum_samples: Vec<_> =
					self.sample_buffer.drain(0..SPECTRUM_WIDTH).collect();
				let res = spectrum_analyzer::samples_fft_to_spectrum(
					&spectrum_samples,
					self.sampling_rate,
					spectrum_analyzer::FrequencyLimit::All,
					self.scaling_fn.as_deref().map(|scaling_fn| {
						scaling_fn as &SpectrumScalingFunction
					}),
				);
				match res {
					Ok(spectrum) => spectra.push(spectrum),
					Err(err) => Self::report_spectrum_analyzer_error(err),
				};
			} else {
				break;
			}
		}

		spectra
	}

	fn update_time(&mut self, timestamp: StreamInstant) {
		let start_time = match self.start_time {
			None => {
				self.start_time = Some(timestamp);
				timestamp
			}
			Some(start_time) => start_time,
		};
		// Don't know why but this is adding an additional field rather than
		// updating the existing field
		// super::super::input::report_timestamp(
		// 	"timestamp",
		// 	start_time,
		// 	timestamp,
		// );
	}

	fn report_spectrum_analyzer_error(error: SpectrumAnalyzerError) {
		use spectrum_analyzer::FrequencyLimitError::*;
		use SpectrumAnalyzerError::*;
		match error {
				TooFewSamples => error!("The spectrum analyzer received too few samples, is the audio buffer size configured correctly?"),
				NaNValuesNotSupported => error!("The spectrum analyzer received NaN samples! Is the audio input stream okay?"),
				InfinityValuesNotSupported => error!("The spectrum analyzer received samples of infinity! Is the autio input stream okay?"),
				InvalidFrequencyLimit(ValueBelowMinimum(limit)) => error!(limit, "The frequency limit can't be below 0!"),
				InvalidFrequencyLimit(ValueAboveNyquist(limit)) => error!(limit, "The frequency limit can't be above the nyquist frequency!"),
				InvalidFrequencyLimit(InvalidRange(lower_limit, upper_limit)) => error!(lower_limit, upper_limit, "Either the limits are below 0 or above nyquist, or the lower limit is larger than the upper limit"),
				SamplesLengthNotAPowerOfTwo => error!("The number of samples provided to the spectrum analyzer was not a power of 2!"),
				ScalingError(before, after) => error!(before, after, "The scaling function returned an infinity or NaN! Is there a bug in the scaling function?"),
			}
	}
}
