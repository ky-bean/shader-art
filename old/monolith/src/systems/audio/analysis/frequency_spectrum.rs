use std::cmp::Ordering;
use std::f32::consts::PI;

use microfft::real::{
	rfft_1024, rfft_128, rfft_16, rfft_16384, rfft_2, rfft_2048, rfft_256,
	rfft_32, rfft_4, rfft_4096, rfft_512, rfft_64, rfft_8, rfft_8192,
};
use num_complex::Complex32;

use crate::systems::audio;

type BinFrequency = f32;
type BinValue = f32;

#[derive(Debug, Copy, Clone, PartialEq)]
pub(crate) struct Bin {
	pub frequency: BinFrequency,
	pub value: BinValue,
}
impl PartialOrd for Bin {
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		match self.frequency.partial_cmp(&other.frequency)? {
			Ordering::Less => Some(Ordering::Less),
			Ordering::Equal => Some(Ordering::Equal),
			Ordering::Greater => Some(Ordering::Greater),
		}
	}
}

#[derive(Debug)]
pub(crate) struct FrequencySpectrum {
	spectrum: Vec<Bin>,
}

// TODO: Write unit tests for the properties defined in here
impl FrequencySpectrum {
	pub fn complex_dft_to_real_dft(spectrum: &[Complex32]) -> Vec<f32> {
		let mut dft = Vec::new();
		for bin in spectrum {
			let (r, theta) = bin.to_polar();
			dft.push(r);
		}
		dft
	}
	pub fn new(spectrum: &[f32], sample_rate: f32) -> Self {
		// TODO: Consider doing this in place (i.e. taking spectrum as &mut and
		// keep it around within `Self`)? Not sure its possible, but since
		// `microfft` does the calculation in place I think it'd be worth giving
		// it a try. Would mean making the things mutable and capture lifetimes
		// means it could get complicated though
		assert!(spectrum.len() != 0);
		assert!(spectrum.len().is_power_of_two());

		let mut freq_spec = Vec::new();
		let fundamental = sample_rate / spectrum.len() as f32;
		for i in 0..(spectrum.len() / 2) {
			let bin_freq = fundamental * i as f32;
			// If this is the first or last bin, take the value of this bin,
			// otherwise take the value of this bin summed with the bin mirrored
			// over spectum.len()/2
			let bin_value = if i == 0 || i == (spectrum.len() / 2 - 1) {
				spectrum[i]
			} else {
				spectrum[i] + spectrum.len() as f32 - i as f32
			};
			freq_spec.push(Bin {
				frequency: bin_freq,
				value: bin_value,
			});
		}

		Self {
			spectrum: freq_spec,
		}
	}

	pub fn average_spectrum(spectra: &[Self]) -> Self {
		let resolution = spectra[0].resolution();
		let num_samples = spectra[0].spectrum.len();
		let sample_rate = spectra[0].sample_rate();
		for spectrum in spectra {
			assert_eq!(spectrum.resolution(), resolution);
			assert_eq!(spectrum.spectrum.len(), num_samples);
		}

		let mut avg_spectrum = Vec::new();

		for i in 0..num_samples {
			let mut bin = Bin {
				frequency: 0.0,
				value: 0.0,
			};
			for spectrum in spectra {
				let spec_bin = spectrum.spectrum[i];
				bin.frequency = spec_bin.frequency;
				bin.value += spec_bin.value;
			}
			bin.value /= spectra.len() as f32;
			avg_spectrum.push(bin);
		}

		Self {
			spectrum: avg_spectrum,
		}
	}

	pub fn spectrum(&self) -> &[Bin] {
		&self.spectrum
	}

	pub fn dc(&self) -> f32 {
		self.spectrum[0].value
	}

	pub fn sample_rate(&self) -> f32 {
		self.resolution() * self.spectrum.len() as f32
	}

	pub fn resolution(&self) -> f32 {
		self.spectrum[1].frequency
	}

	pub fn average(&self) -> f32 {
		let len = self.spectrum.len() as f32;
		self.sum() / len
	}

	pub fn sum(&self) -> f32 {
		self.spectrum_iter_skip_dc().map(|bin| bin.value).sum()
	}

	pub fn max(&self) -> f32 {
		self.spectrum_iter_skip_dc()
			.map(|bin| bin.value)
			.fold(0.0, |max, val| if val > max { val } else { max })
	}

	fn spectrum_iter_skip_dc(&self) -> impl Iterator<Item = Bin> + '_ {
		self.spectrum.iter().skip(1).copied()
	}
}

macro_rules! dft_binary_len {
    ($dft_fn:ident<$binary:literal>($samples:expr) $(,)?) => {
        ensure_array_length!($dft_fn<$binary>($samples))
    };
    ($dft_fn:ident<$binary:literal>($samples:expr), $($tt:tt)*) => {
        ensure_array_length!($dft_fn<$binary>($samples)).or_else(|| dft_binary_len!($($tt)*))
    };
}
macro_rules! ensure_array_length {
	($dft_fn:ident<$binary:literal>($samples:expr)) => {
	    // FIXME: This is an inlined implementation of the proposed API change #95198 which is due
	    // to be stabalized when https://github.com/rust-lang/rust/pull/117561 merges. Should
	    // change this to that when it's stable.
		if $samples.len() == $binary {
		    // Safety: The length is explicitly checked above, the lifetime is bound to the
		    // original slice's lifetime, and is behind a mutable reference ensuring exclusive
		    // access.
		    let array = unsafe{&mut *($samples.as_mut_ptr() as *mut [f32; $binary])};
			Some(AsMut::<[_]>::as_mut($dft_fn(array)))
		} else {
			None
		}
	};
}
// TODO: Move this to the construction process of `FrequencySpectrum`.
fn dft(
	samples: &mut [audio::Sample],
	sample_rate: f32,
) -> Option<FrequencySpectrum> {
	let spectrum = dft_binary_len! {
		rfft_2<2>(samples),
		rfft_4<4>(samples),
		rfft_8<8>(samples),
		rfft_16<16>(samples),
		rfft_32<32>(samples),
		rfft_64<64>(samples),
		rfft_128<128>(samples),
		rfft_256<256>(samples),
		rfft_512<512>(samples),
		rfft_1024<1024>(samples),
		rfft_2048<2048>(samples),
		rfft_4096<4096>(samples),
		rfft_8192<8192>(samples),
		rfft_16384<16384>(samples),
	}?;

	Some(FrequencySpectrum::new(
		&FrequencySpectrum::complex_dft_to_real_dft(spectrum),
		sample_rate,
	))
}
