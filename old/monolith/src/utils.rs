// TODO: Put everything in this module into their own modules in their
// respective places throughout the engine.
use glam::{Vec2, Vec3};
use tracing::trace;
use winit::dpi::{PhysicalPosition, PhysicalSize};

pub(crate) fn srgb(rgb: [f32; 3]) -> [f32; 3] {
	fn convert(c: f32) -> f32 {
		((c + 0.055) / 1.055).powf(2.4)
	}

	let srgb = [convert(rgb[0]), convert(rgb[1]), convert(rgb[2])];
	trace!(?rgb, ?srgb, "Converting rgb color to srgb");

	srgb
}
pub(crate) fn rgb(srgb: [f32; 3]) -> [f32; 3] {
	fn convert(c: f32) -> f32 {
		c.powf(1. / 2.4) * 1.055 - 0.055
	}

	let rgb = [convert(srgb[0]), convert(srgb[1]), convert(srgb[2])];
	trace!(?srgb, ?rgb, "Converting srgb color to rgb");

	rgb
}

pub(crate) fn mul_color(color: wgpu::Color, scalar: f64) -> wgpu::Color {
	wgpu::Color {
		r: color.r * scalar,
		g: color.g * scalar,
		b: color.b * scalar,
		a: color.a,
	}
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub(crate) struct Hsv {
	hue: f32,
	saturation: f32,
	value: f32,
}

impl Hsv {
	pub fn new(hue: f32, saturation: f32, value: f32) -> Self {
		Self {
			hue,
			saturation,
			value,
		}
	}

	// hue: [0, 1], saturation: [0, 1], value: [0, 1]
	pub fn to_rgba(self, alpha: f32) -> [f32; 4] {
		let rgb = self.to_rgb();
		[rgb[0], rgb[1], rgb[2], alpha]
	}

	pub fn to_rgb(self) -> [f32; 3] {
		let Self {
			hue,
			saturation,
			value,
		} = self;

		let hue = (hue as f32 % 1.) * 360.;
		let h_prime = hue / 60.;

		let c = saturation as f32 * value as f32;
		let x = c * (1. - (h_prime % 2. - 1.).abs());

		let partial_rgb = match h_prime {
			h if h >= 0. && h < 1. => [c, x, 0.],
			h if h >= 1. && h < 2. => [x, c, 0.],
			h if h >= 2. && h < 3. => [0., c, x],
			h if h >= 3. && h < 4. => [0., x, c],
			h if h >= 4. && h < 5. => [x, 0., c],
			h if h >= 5. && h < 6. => [c, 0., x],
			_ => unreachable!("hue is clamped to being within [0, 1], scaled up by 360, and back down by 60 to a range of [0, 6]"),
		};

		let m = value as f32 - c;

		let rgb = [partial_rgb[0] + m, partial_rgb[1] + m, partial_rgb[2] + m];
		rgb
	}

	pub fn from_rgb([r, g, b]: [f32; 3]) -> Self {
		let x_max = r.max(g).max(b);
		let x_min = r.min(g).min(b);

		let chroma = x_max - x_min;
		let value = x_max;

		let hue = if chroma == 0. {
			0.
		} else {
			match value {
				v if v == r => 60. * (((g - b) / chroma) % 6.),
				v if v == g => 60. * (((b - r) / chroma) + 2.),
				v if v == b => 60. * (((r - g) / chroma) + 4.),
				_ => unreachable!("v must be one of r, g, or b, so one of the branches is guaranteed to be hit"),
			}
		};

		let saturation = if value == 0. { 0. } else { chroma / value };

		Self::new(hue, saturation, value)
	}
	pub fn from_rgba([r, g, b, _a]: [f32; 4]) -> Self {
		Self::from_rgb([r, g, b])
	}
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub(crate) struct Polar {
	radius: f32,
	// [-π, +π]
	theta: f32,
}
impl Polar {
	pub fn new(radius: f32, theta: f32) -> Self {
		Self { radius, theta }
	}
	pub fn radius(&self) -> f32 {
		self.radius
	}
	pub fn theta(&self) -> f32 {
		self.theta
	}
}

pub(crate) fn cursor_pos_polar(
	position: PhysicalPosition<f64>,
	dims: PhysicalSize<u32>,
) -> Polar {
	let center = Vec2::new((dims.width as f32) / 2., (dims.height as f32) / 2.);
	let position = Vec2::new(position.x as f32, position.y as f32);

	let smaller_dim = center.x.min(center.y);

	let vector = position - center;
	if vector == Vec2::ZERO {
		Polar::new(0., 0.)
	} else {
		let theta = Vec2::Y.angle_between(vector); // vector.angle_between(DVec2::Y);
		let radius = (vector.length() / smaller_dim).min(1.);

		Polar::new(radius, theta)
	}
}
pub(crate) fn rgba([r, g, b, a]: [f32; 4]) -> wgpu::Color {
	wgpu::Color {
		r: r as f64,
		g: g as f64,
		b: b as f64,
		a: a as f64,
	}
}
