mod frequency_spectrum;
mod spectrum_processor;

pub(super) use frequency_spectrum::FrequencySpectrum;
pub(super) use spectrum_processor::SpectrumProcessor;
