#![allow(unused_imports)]

use std::error::Error;

use tracing::{info, Level};
use tracing_subscriber::{EnvFilter, FmtSubscriber};

fn main() -> Result<(), Box<dyn Error>> {
	// set up `tracing`
	let subscriber = FmtSubscriber::builder()
		.with_env_filter(EnvFilter::from_env("LOG_LEVEL"))
		.finish();
	tracing::subscriber::set_global_default(subscriber)
		.expect("setting default subscriber failed");

	info!("Tracing initialized");

	// run the program
	pollster::block_on(leitmo::run());

	Ok(())
}
