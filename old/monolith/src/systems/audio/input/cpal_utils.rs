use std::collections::BTreeSet;
use std::sync::mpsc;

use cpal::traits::{DeviceTrait, HostTrait};
use cpal::{
	BufferSize, Host, HostId, SampleRate, StreamConfig, SupportedStreamConfig,
	SupportedStreamConfigRange,
};
use tracing::{debug, event, info};
use tracing_unwrap::{OptionExt, ResultExt};

use super::cpal_handler::InputPayload;
use super::{CpalStreamErrorHandler, CpalStreamHandler};
use crate::systems::audio::SamplesBuf;

const DEBUG: tracing::Level = tracing::Level::DEBUG;
const TRACE: tracing::Level = tracing::Level::TRACE;
const INFO: tracing::Level = tracing::Level::INFO;

macro_rules! trace_hosts {
	($level:expr) => {
		let hosts = cpal::available_hosts();
        event!($level, "Available hosts:");
		for host in hosts {
			let id = host;
			let name = host.name();
			event!($level, ?id, name);
		}
	};
}
macro_rules! trace_devices {
	($level:expr, $host:expr) => {
		let host = $host;
		let devices = host.input_devices().unwrap_or_log();
		event!($level, "Devices available to host {}:", host.id().name());
		for device in devices {
			let name = device.name().unwrap_or_log();
			event!($level, name);
		}
	};
}
macro_rules! trace_output_devices {
    ($level:expr, $host:expr) => {
        let host = $host;
        let devices = host.output_devices().unwrap_or_log();
        event!($level, "Output devices available to host {}:", host.id().name());
        for device in devices {
            let name = device.name().unwrap_or_log();
            event!($level, name);
        }
    };
}
macro_rules! trace_configs {
	($level:expr, $device:expr) => {
		let device = $device;
		let configs = device.supported_input_configs().unwrap_or_log();
		event!(
			$level,
			"Configs available to device {}:",
			device.name().unwrap_or_log()
		);
		trace_config_range!($level, configs);
	};
}
macro_rules! trace_config_range {
	($level:expr, $configs:expr) => {
		for config in $configs {
			let channels = config.channels();
			let min_sample_rate = config.min_sample_rate().0;
			let max_sample_rate = config.max_sample_rate().0;
			let buffer_size = config.buffer_size();
			let sample_format = config.sample_format();
			event!(
				$level,
				channels,
				min_sample_rate,
				max_sample_rate,
				?buffer_size,
				?sample_format,
			);
		}
	};
}
macro_rules! trace_config {
	($level:expr, $config:expr) => {
		let config = $config;
		let channels = config.channels();
		let sample_rate = config.sample_rate().0;
		let buffer_size = config.buffer_size();
		let sample_format = config.sample_format();
		event!($level, channels, sample_rate, ?buffer_size, ?sample_format,);
	};
}

pub(in crate::systems::audio) fn init_input_stream(
	sender: mpsc::Sender<InputPayload>,
) -> (cpal::StreamConfig, cpal::Stream) {
	trace_hosts!(INFO);
    //#[cfg(target_os = "windows")]
	let host = cpal::default_host();
    // let mut host_preference = cpal::available_hosts();
    // host_preference.retain(|id| match id.name().to_lowercase().as_str() {
    //     "alsa" | "jack" => true,
    //     _ => false
    // });
    // host_preference.sort_by(|l, r| std::cmp::Ordering::Greater);
    // let host = host_preference.into_iter().map(cpal::host_from_id).flatten().next().unwrap_or_else(|| cpal::default_host());
	info!("Selecting host {}", host.id().name());

	trace_devices!(INFO, &host);
    //trace_output_devices!(INFO, &host);
	let device = host
		.default_input_device()
		.expect("No audio input device available");
    //let device = host.default_output_device().expect("Linux makes me sad :(");
    // let device = host.output_devices().expect_or_log("Trying this out").find(|dev| dev.name().unwrap_or_log() == "pipewire").unwrap_or_log();
    // let device = host
    //     .input_devices()
    //     .expect_or_log("no input devices?")
    //     .find(|dev| dev.name().unwrap_or_log() == "sysdefault:CARD=UR22mkII")
    //     .unwrap_or_log();
	info!("Selecting device {}", device.name().unwrap_or_log());

	// // trace_configs!(INFO, &device);
	// let supported_configs_range = device
	// 	.supported_input_configs()
	// 	.expect_or_log("error while querying audio configs");
	//
	// // FIXME: Use a different methodology for selecting a config than the
	// // default. It is not the greatest default configuration in practice,
	// // and I'll eventually want to be able to specify a config file of some
	// // kind to either cache a CLI-provided config or as a user-editable
	// // config (or both?).
	// // let mut supported_configs: Vec<_> = supported_configs_range
	// // 	.filter(|config| {
	// // 		!matches!(config.sample_format(), cpal::SampleFormat::U8)
	// // 	})
	// // 	.collect();
    // let mut iter = supported_configs_range.into_iter();
	// // let mut iter = iter.filter(|config| {
	// // 	matches!(config.sample_format(), cpal::SampleFormat::I32)
	// // });
	// let mut iter = iter.filter(|config| {
	// 	config.min_sample_rate().0 > 44_100
	// 		&& config.max_sample_rate().0 > 96_000
	// });
    // let mut supported_configs: Vec<_> = iter.collect();
	// trace_config_range!(INFO, supported_configs.as_slice());
	// supported_configs
	// 	.sort_by(SupportedStreamConfigRange::cmp_default_heuristics);
    // let supported_config = supported_configs[0];
	// // let supported_config = iter
	// // 	.next()
	// // let supported_config = supported_configs
	// // 	.drain(0..1)
	// // 	.next()
	// 	// .expect_or_log("no supported audio config?!");
	// // let supported_config = SupportedStreamConfig::new(1, 96_000, 16_384)
	// // trace_config!(INFO, &supported_config);
	//
	// // FIXME: Without specifying a buffer size for the config, there's a
	// // possibility that the generated stream will use a large buffer size
	// // that can lead to latency issues. Given I have observed that the
	// // default behavior has selected a config with a sample rate of *384kHz*
	// // it would not surprise me if it also picked a high buffer size so
	// // that's something to explore in the future. We also need to specify
	// // the buffer size to ensure that it is a power of 2, which is needed
	// // for `spectrum-analyzer`'s FFT algorithm.
	// let config = supported_config.with_max_sample_rate().config();
    let rate = 192_000;
	let config = StreamConfig {
		channels: 1,
		sample_rate: SampleRate(rate),//SampleRate(48_000 * 8),
		buffer_size: BufferSize::Fixed(rate / 4),//BufferSize::Fixed(16_384),
	};

	let mut input_handler = CpalStreamHandler::new(sender);
	let mut error_handler = CpalStreamErrorHandler::new();

	let stream = device
		.build_input_stream(
			&config,
			move |data: &[f32], info: &cpal::InputCallbackInfo| {
				input_handler.process_input(data, info);
			},
			move |err| {
				error_handler.process_error(err);
			},
			None,
		)
		.expect_or_log("building audio input stream failed");

	(config, stream)
}
