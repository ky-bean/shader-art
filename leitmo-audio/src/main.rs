use clap::Parser;
// use tracing::{info, Level};
// use tracing_subscriber::{EnvFilter, FmtSubscriber};

#[derive(Debug, Parser)]
#[command(version, about, long_about = None)]
struct Cli {
	#[arg(short, long)]
	verbosity: u8,
	#[arg(long)]
	display_audio_configs: bool,
}

fn main() {
}
