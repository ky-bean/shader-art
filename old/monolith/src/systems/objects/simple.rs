use super::object::Object;
use crate::systems::graphics::Mesh;

#[derive(Debug)]
pub struct Simple {
	mesh: Mesh,
}
// TODO: Add constructors for simple shapes.
impl Simple {
	pub fn new(mesh: Mesh) -> Self {
		Self { mesh }
	}
}
impl Object for Simple {
	fn update(&mut self, dt: f32) {
		// Do nothing...
	}

	fn mesh(&self) -> Option<&Mesh> {
		Some(&self.mesh)
	}

	fn mesh_mut(&mut self) -> Option<&mut Mesh> {
		Some(&mut self.mesh)
	}
}
