pub(super) mod cpal_handler;
pub(super) mod cpal_utils;

use std::time::Duration;

use cpal::StreamInstant;
pub(super) use cpal_handler::{CpalStreamErrorHandler, CpalStreamHandler};
pub(super) use cpal_utils::init_input_stream;

pub(super) fn report_timestamp(
	record_name: &str,
	start_time: StreamInstant,
	current_time: StreamInstant,
) -> Option<Duration> {
	let duration = current_time.duration_since(&start_time);
	if let Some(duration) = duration {
		let span = tracing::Span::current();
		span.record(record_name, duration.as_secs_f32());
		Some(duration)
	} else {
		None
	}
}
