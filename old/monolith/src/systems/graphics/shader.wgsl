//TODO: clean up and organize this, possibly separate vertex and fragment shaders into separate files

struct VertexInput {
	@location(0) position: vec3<f32>,
	@location(1) viewport_dims: vec2<u32>,
	@location(2) dt: f32,
	@location(3) elapsed: f32,
	@location(4) freq_scalar: f32,
	@location(5) phase_scalar: f32,
	@location(6) phase: f32,
	@location(7) minimum: f32,
	@location(8) bandwidth: f32,
	@location(9) color_param: f32,
	@location(10) energy: f32,
	@location(11) depth_of_field: f32,
	@location(12) recurse_depth: f32,
}
struct VertexOutput {
	@builtin(position) clip_position: vec4<f32>,
	@location(0) viewport_dims: vec2<u32>,
	@location(1) dt: f32,
	@location(2) elapsed: f32,
	@location(3) freq_scalar: f32,
	@location(4) phase_scalar: f32,
	@location(5) phase: f32,
	@location(6) minimum: f32,
	@location(7) bandwidth: f32,
	@location(8) color_param: f32,
	@location(9) energy: f32,
	@location(10) depth_of_field: f32,
	@location(11) recurse_depth: f32,
};

fn rgb2hsv(rgb: vec3<f32>) -> vec3<f32> {
	let x_max = max(rgb[0], max(rgb[1], rgb[2]));
	let x_min = min(rgb[0], min(rgb[1], rgb[2]));

	let chroma = x_max - x_min;
	let val = x_max;

	var hue: f32;
	if chroma == 0.0 { hue = 0.0; }
	else if val == rgb[0] { hue = 60.0 * (((rgb[1] - rgb[2]) / chroma) % 6.0); }
	else if val == rgb[1] { hue = 60.0 * (((rgb[2] - rgb[0]) / chroma) + 2.0); }
	else { hue = 60.0 * (((rgb[0] - rgb[1]) / chroma) + 4.0); }

	var sat: f32;
	if val == 0.0 { sat = 0.0; }
	else { sat = chroma / val; }

	return vec3(hue / 360.0, sat, val);
}

// assumes h: [0, 1], s: [0, 1], v: [0, 1]
fn hsv2rgb(hsv: vec3<f32>) -> vec3<f32> {
	return vec3(
			inner_hsv_calc(5.0, hsv),
			inner_hsv_calc(3.0, hsv),
			inner_hsv_calc(1.0, hsv)
			);
}
fn inner_hsv_calc(n: f32, hsv: vec3<f32>) -> f32 {
	let h = hsv[0];
	let s = hsv[1];
	let v = hsv[2];
	let k = (n + h*6.0) % 6.0;
	return v-v*s*max(0.0, min(k, min(4.0 - k, 1.0)));
}
const PI: f32 = 3.14159265358979323846264338327950288;
const TAU: f32 = 6.28318530717958647692528676655900577;
const E: f32 = 2.71828182845904523536028747135266250;
fn sin01(in: f32) -> f32 {
	return sin(in)/2.0 + 0.5;
}

fn mod_color(color: vec3<f32>, hue_rate: f32, time_elapsed: f32) -> vec3<f32> {
	let hsv = rgb2hsv(color);
	let hsv_mod = vec3(hsv.x + hue_rate*time_elapsed, hsv.yz);
	return hsv2rgb(hsv_mod);
}

fn ease_logarithmic(input: f32, base: f32) -> f32 {
	if base <= 1.0 {
		return input;
	}

	let log_param = (base - 1.0 / base) * input + 1.0 / base;
	let log_out = log(log_param)/log(base);

	return (log_out + 1.0) / 2.0;
}

// @vertex marks this as an entry point for a vertex shader. It takes a u32 from @builtin(vertex_index)
@vertex
fn vs_main(model: VertexInput) -> VertexOutput {
	// var bindings are mutable, let bindings are immutable
	var out: VertexOutput;

	out.clip_position = vec4<f32>(model.position, 1.0);
	out.viewport_dims = model.viewport_dims;
	out.dt = model.dt;
	out.elapsed = model.elapsed;
	out.freq_scalar = model.freq_scalar;
	out.phase_scalar = model.phase_scalar;
	out.phase = model.phase;
	out.minimum = model.minimum;
	out.bandwidth = model.bandwidth;
	out.color_param = model.color_param;
	out.energy = model.energy;
	out.depth_of_field = model.depth_of_field;
	out.recurse_depth = model.recurse_depth;
	
	return out;
}

fn calc_uv(pos: vec2<f32>, dims: vec2<f32>) -> vec2<f32> {
  var uv = pos / dims;
  uv.y = 1.0 - uv.y;
	uv = uv * 2.0 - 1.0;
	if dims.x > dims.y {
		uv.x *= dims.x / dims.y;
	} else {
		uv.y *= dims.y / dims.x;
	}
	return uv;
}

fn lerp(a: vec3f, b: vec3f, t: f32) -> vec3f {
	return a + t * (b - a);
}

// See: https://iquilezles.org/articles/palettes/
// Find nice values with: http://dev.thi.ng/gradients/
fn palette(t: f32) -> vec3f {
  let a = vec3f(0.75, 1.25, 1.0);
  let b = vec3(0.75, 1.0, -0.125);
  let c = vec3(0.325, 0.325, 0.75);
  let d = vec3(-0.875, 2.5, 1.0);

  var color = a + b * cos(TAU * (c*t + d));

  color.y = min(color.y, 1.0);

	return color;
}

fn alt_palette(t: f32) -> vec3f {
  let color_scalar = 255.0;
	let a = vec3f(97.0, 67.0, 118.0) / color_scalar;
	let b = vec3f(75.0, 218.0, 253.0) / color_scalar;
  let c = vec3f(146.0, 45.0, 53.0) / color_scalar;
  let d = vec3f(189.0, 88.0, 185.0) / color_scalar;
  
  let tn = t % 1.0;

	var color: vec3f;

  if tn < 0.25 {
  	color = lerp(a, b, tn / 0.25);
  } else if tn < 0.5 {
  	color = lerp(b, c, (tn - 0.25) / 0.25);
  } else if tn < 0.75 {
  	color = lerp(c, d, (tn - 0.5) / 0.25);
  } else if tn < 1.0 {
  	color = lerp(d, a, (tn - 0.75) / 0.25);
  }

  return color;
}

fn sample(uv: vec2f, len_uv0: f32, params: VertexOutput, i: f32) -> vec3f {
	let len_uv = length(uv);

	// https://www.desmos.com/calculator/sy2ze5gef9
	var x = len_uv * exp(-2.0*len_uv0);

	let freq = params.freq_scalar * (1.0 + len_uv0) * PI;
	x = abs(sin(freq * x + params.phase));

	let power = 1.0 / (log2(abs(sin(params.bandwidth * PI)))/log2(params.minimum));
	x = pow(x, power);

	x = params.minimum / x;

	var color_scalar = x * max(params.energy, 0.1);
	var color = color_scalar * alt_palette(len_uv0 * params.color_param + (params.elapsed + i) * 0.4);

	return color;
}

// @fragment marks this as an entry point for a fragment shader
@fragment
// @location(0) tells WGPU to store the vec4 returned from here in the first color target
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
  var params = in;
	let viewport = vec2f(params.viewport_dims);
	let uv0 = calc_uv(params.clip_position.xy, viewport);
	let grayscale = vec3f(0.0625 / 2.0);
	var final_color = grayscale * params.energy;
	//var final_color = vec3f(0.0);
	var uv = uv0;

	let len_uv0 = length(uv0);

  for (var i: f32 = 1.0; i <= params.recurse_depth; i+= 1.0) {
  	// TODO: This operation is what divides the viewport into sections. Do some fancy
  	// stuff with this by thinking in Complex Number space (Z plane if i remember right?)
		uv = fract(uv * params.depth_of_field) - 0.5;

		var color = vec3f();
		
		color += sample(uv, len_uv0, params, i);

		final_color += color;
	}

	return vec4<f32>(final_color, 1.0);
}
