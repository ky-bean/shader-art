use super::{Vec2, Vec3};
use crate::systems::audio::SignalParams;
use crate::time::TimePoint;

#[derive(
	Debug,
	Default,
	Copy,
	Clone,
	PartialEq,
	PartialOrd,
	bytemuck::Pod,
	bytemuck::Zeroable,
)]
#[repr(C)]
pub struct ShaderParams {
	pub(super) time: TimePoint,
	pub(super) freq_scalar: f32,
	pub(super) phase_scalar: f32,
	pub(super) phase: f32,
	pub(super) minimum: f32,
	pub(super) bandwidth: f32,
	pub(super) color_param: f32,
	pub(super) energy: f32,
	pub(super) depth_of_field: f32,
	pub(super) recurse_depth: f32,
}

impl ShaderParams {
	pub fn new(time: TimePoint, signal_params: SignalParams) -> Self {
		let freq_scalar = 6.0;

		let phase_scalar = 2.0;
		let phase = std::f32::consts::PI
			* (time.elapsed()
				- (signal_params.user2() + signal_params.user4()) * 0.25)
			/ phase_scalar;

		let minimum = 0.05;
		let bandwidth = 0.325 * signal_params.user1().max(0.05);
		let color_param = signal_params.user3(); //(signal_params.user3() + time.elapsed()) * 0.4;
		let energy = signal_params.energy();
		let depth_of_field = 1.625;
		let recurse_depth = 3.0;

		Self {
			time,
			freq_scalar,
			phase_scalar,
			phase,
			minimum,
			bandwidth,
			color_param,
			energy,
			depth_of_field,
			recurse_depth,
		}
	}
}
