#![doc = include_str!("../README.md")]
#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(dead_code)]
#![deny(unsafe_op_in_unsafe_fn)]
#![deny(clippy::undocumented_unsafe_blocks)]

use std::error::Error;

use tracing::{
	error, error_span, info, info_span, trace, trace_span, warn, warn_span,
	Level,
};
use tracing_subscriber::FmtSubscriber;
use tracing_unwrap::{OptionExt, ResultExt};

#[tracing::instrument]
pub async fn run() {
}
