#![doc = include_str!("../README.md")]
#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(dead_code)]
#![deny(unsafe_op_in_unsafe_fn)]
#![deny(clippy::undocumented_unsafe_blocks)]

mod ease;
mod engine;
mod systems;
mod time;
mod utils;

use std::error::Error;

use tracing::{
	error, error_span, info, info_span, trace, trace_span, warn, warn_span,
	Level,
};
use tracing_subscriber::FmtSubscriber;
use tracing_unwrap::{OptionExt, ResultExt};
use winit::dpi::{LogicalSize, PhysicalSize};
use winit::event::*;
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::WindowBuilder;

use crate::engine::Engine;
use crate::systems::graphics::{Mesh, Vertex};

#[tracing::instrument]
pub async fn run() {
	// Initialize the window
	let event_loop = EventLoop::new();
	// For whatever reason the window that spawns has an inner size of this +
	// 20, idk why.
	// let dim = 960 - 20;
	let window = WindowBuilder::new()
		// .with_inner_size(PhysicalSize::new(dim, dim))
		.build(&event_loop)
		.expect_or_log("Failed to build window");

	window.set_title("The Experiment");
    window.set_inner_size(PhysicalSize::new(600,510));

	// Build the engine
	let mut engine = Engine::new(window).await;
	info!("Engine initialized");

	info!("Entering main loop");
	// Run the engine
	event_loop.run(move |event, _, control_flow| {
		engine.handle_winit_event(event, control_flow)
	});
}
