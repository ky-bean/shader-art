use std::mem::size_of;

use super::buffers::RawBuffer;
use super::shader_params::ShaderParams;
use super::{Vec2, Vec3};
use crate::systems::audio::SignalParams;
use crate::utils::Hsv;

// `Pod` indicates "plain-old data"
// `Zeroable` indicates that `std::mem::zeroed()` can be used
#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub(crate) struct Vertex {
	position: Vec3<f32>,      // x, y, z
	viewport_dims: Vec2<u32>, // width, height
	shader_params: ShaderParams,
}

const SQRT_3: f32 = 1.732050807568877293527446341505872367_f32;
const SIZE_F32: usize = size_of::<f32>();
const SIZE_VEC2_U32: usize = size_of::<Vec2<u32>>();
const SIZE_VEC3_F32: usize = size_of::<Vec3<f32>>();

impl Vertex {
	pub(crate) fn new(position: Vec3<f32>, viewport_dims: Vec2<u32>) -> Self {
		Self {
			position,
			viewport_dims,
			shader_params: Default::default(),
		}
	}

	pub fn update_viewport_dims(&mut self, viewport_dims: Vec2<u32>) {
		self.viewport_dims = viewport_dims;
	}

	pub fn update(&mut self, shader_params: ShaderParams) {
		self.shader_params = shader_params;
	}

	// TODO: Break these out of here and be defined somewhere else like maybe
	// `objects::Simple`.
	// pub(crate) fn square() -> RawBuffer {
	// 	let color = [1.0, 1.0, 1.0];
	// 	RawBuffer::new(
	// 		vec![
	// 			Vertex::new([-0.5, -0.5, 0.0], color),
	// 			Vertex::new([0.5, -0.5, 0.0], color),
	// 			Vertex::new([0.5, 0.5, 0.0], color),
	// 			Vertex::new([-0.5, 0.5, 0.0], color),
	// 		]
	// 		.leak(),
	// 		vec![0, 2, 3, 0, 1, 2].leak(),
	// 	)
	// }
	pub(crate) fn square(viewport_dims: Vec2<u32>) -> RawBuffer {
		let color = [1.0, 1.0, 1.0];
		let size = 1.0;
		RawBuffer::new(
			vec![
				Vertex::new([-size, -size, 0.0], viewport_dims),
				Vertex::new([size, -size, 0.0], viewport_dims),
				Vertex::new([size, size, 0.0], viewport_dims),
				Vertex::new([-size, size, 0.0], viewport_dims),
			]
			.leak(),
			vec![0, 2, 3, 0, 1, 2].leak(),
		)
	}

	// TODO: Find and/or make an attribute macro that automatically generates
	// this from the struct definition. Really feel like this should be a thing
	// already so focus more on searching than trying to also learn the rust
	// macro system, as simple as the macro would probably be.
	//
	// TODO: This is now made even more periless with moving the bulk of the
	// data to a separate struct, decoupling it from this file.
	const ATTRIBUTES: [wgpu::VertexAttribute; 13] = [
		// position
		wgpu::VertexAttribute {
			// Defines the `offset` in bytes until the attribute starts. First
			// attribute offset is usually 0, and following attributes are
			// `size_of` * the number of previous attributes.
			offset: 0,
			// Tells the shader what location to store this attribute at.
			// `@location(0) x: vec3<f32>` in the vertex shader would correspond
			// to the `position` field of the `Vertex` struct and `@location(1)
			// x: vec3<f32>` would be the `color` field.
			shader_location: 0,
			// Tells the shader the shape of the attribute. `Float32x3`
			// corresponds to `vec3<f32>` in shader code.
			format: wgpu::VertexFormat::Float32x3,
		},
		// viewport_dims
		wgpu::VertexAttribute {
			offset: SIZE_VEC3_F32 as wgpu::BufferAddress,
			shader_location: 1,
			format: wgpu::VertexFormat::Uint32x2,
		},
		// dt
		wgpu::VertexAttribute {
			offset: (SIZE_VEC3_F32 + SIZE_VEC2_U32) as wgpu::BufferAddress,
			shader_location: 2,
			format: wgpu::VertexFormat::Float32,
		},
		// elapsed
		wgpu::VertexAttribute {
			offset: (SIZE_VEC3_F32 + SIZE_VEC2_U32 + SIZE_F32)
				as wgpu::BufferAddress,
			shader_location: 3,
			format: wgpu::VertexFormat::Float32,
		},
		// freq_scalar
		wgpu::VertexAttribute {
			offset: (SIZE_VEC3_F32 + SIZE_VEC2_U32 + 2 * SIZE_F32)
				as wgpu::BufferAddress,
			shader_location: 4,
			format: wgpu::VertexFormat::Float32,
		},
		// phase_scalar
		wgpu::VertexAttribute {
			offset: (SIZE_VEC3_F32 + SIZE_VEC2_U32 + 3 * SIZE_F32)
				as wgpu::BufferAddress,
			shader_location: 5,
			format: wgpu::VertexFormat::Float32,
		},
		// phase
		wgpu::VertexAttribute {
			offset: (SIZE_VEC3_F32 + SIZE_VEC2_U32 + 4 * SIZE_F32)
				as wgpu::BufferAddress,
			shader_location: 6,
			format: wgpu::VertexFormat::Float32,
		},
		// minimum
		wgpu::VertexAttribute {
			offset: (SIZE_VEC3_F32 + SIZE_VEC2_U32 + 5 * SIZE_F32)
				as wgpu::BufferAddress,
			shader_location: 7,
			format: wgpu::VertexFormat::Float32,
		},
		// bandwidth
		wgpu::VertexAttribute {
			offset: (SIZE_VEC3_F32 + SIZE_VEC2_U32 + 6 * SIZE_F32)
				as wgpu::BufferAddress,
			shader_location: 8,
			format: wgpu::VertexFormat::Float32,
		},
		// color_param
		wgpu::VertexAttribute {
			offset: (SIZE_VEC3_F32 + SIZE_VEC2_U32 + 7 * SIZE_F32)
				as wgpu::BufferAddress,
			shader_location: 9,
			format: wgpu::VertexFormat::Float32,
		},
		// energy
		wgpu::VertexAttribute {
			offset: (SIZE_VEC3_F32 + SIZE_VEC2_U32 + 8 * SIZE_F32)
				as wgpu::BufferAddress,
			shader_location: 10,
			format: wgpu::VertexFormat::Float32,
		},
		// depth_of_field
		wgpu::VertexAttribute {
			offset: (SIZE_VEC3_F32 + SIZE_VEC2_U32 + 9 * SIZE_F32)
				as wgpu::BufferAddress,
			shader_location: 11,
			format: wgpu::VertexFormat::Float32,
		},
		// recurse_depth
		wgpu::VertexAttribute {
			offset: (SIZE_VEC3_F32 + SIZE_VEC2_U32 + 10 * SIZE_F32)
				as wgpu::BufferAddress,
			shader_location: 12,
			format: wgpu::VertexFormat::Float32,
		},
	];

	// TODO: See `Self::ATTRIBUTES`.
	pub(crate) fn desc() -> wgpu::VertexBufferLayout<'static> {
		wgpu::VertexBufferLayout {
			// Defines how wide a vertex is.
			array_stride: size_of::<Self>() as wgpu::BufferAddress,
			// Tells the pipeline whether each element of the array in the
			// buffer represents per-vetex data or per-instance data.
			step_mode: wgpu::VertexStepMode::Vertex,
			// Vertex attributes describe the individual parts of the vertex.
			// Generally a 1:1 mapping with a struct's fields, which is true in
			// this case.
			attributes: &Self::ATTRIBUTES,
		}
	}
}
