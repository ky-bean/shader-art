{
	description = "Leitmo project";

	inputs = {
		nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
	};

	outputs = { self, nixpkgs, ... }:
	let
		system = "x86_64-linux";
		pkgs = nixpkgs.legacyPackages.${system};
		buildPkgs = with pkgs; [
				pkg-config

				# audio libs
				alsa-lib
				jack2

				# graphics libs
				xorg.libX11
				xorg.libXcursor
				xorg.libXrandr
				xorg.libXi
				libxkbcommon
				libGL
				vulkan-loader
				wayland
				xwayland

				# provides libudev, needed for bevy
				libudev-zero
			];
		libPath = with pkgs; lib.makeLibraryPath buildPkgs;
	in {
		devShells.x86_64-linux.default = pkgs.mkShell {
			nativeBuildInputs = [ pkgs.pkg-config ];
			buildInputs = buildPkgs;
			LD_LIBRARY_PATH = libPath;
		};
	};
}
