{...}: {
  perSystem = {
    pkgs,
    config,
    ...
  }: let
    crateName = "sanity-check";
  in {
    # declare projects
    nci.projects.${crateName}.path = ./.;
    # configure crates
    nci.crates.${crateName} = rec {
		depsDrvConfig = {
			mkDerivation = {
				nativeBuildInputs = with pkgs; [ pkg-config ];
				buildInputs = with pkgs; [alsa-lib];
			};
		};
		drvConfig = {mkDerivation = {
			nativeBuildInnputs = [pkgs.pkg-config];
			buildInputs = depsDrvConfig.mkDerivation.buildInputs;
		};};
	};
  };
}
