use std::collections::VecDeque;
use std::time::Instant;

use tracing::info;

#[derive(
	Debug,
	Default,
	Copy,
	Clone,
	PartialEq,
	PartialOrd,
	bytemuck::Pod,
	bytemuck::Zeroable,
)]
#[repr(C)]
pub struct TimePoint {
	pub(crate) dt: f32,
	pub(crate) elapsed: f32,
}
impl TimePoint {
	pub fn dt(&self) -> f32 {
		self.dt
	}
	pub fn elapsed(&self) -> f32 {
		self.elapsed
	}
}

#[derive(Debug)]
pub(crate) struct TimeTracker<const WINDOW_SIZE: usize = 4096> {
	dt: f32,
	elapsed: f32,
	frame_count: usize,
	last_update: Option<Instant>,
	// TODO: Change this to use an array-backed deque thing probably.
	dt_window: VecDeque<f32>,
}
impl<const WINDOW_SIZE: usize> TimeTracker<WINDOW_SIZE> {
	#[inline]
	pub fn new() -> Self {
		Self {
			dt: 0.0,
			elapsed: 0.0,
			frame_count: 0,
			last_update: None,
			dt_window: VecDeque::from([f32::INFINITY; WINDOW_SIZE]),
		}
	}
	#[inline]
	pub fn reset(&mut self) {
		self.dt = 0.0;
		self.elapsed = 0.0;
		self.frame_count = 0;
		self.last_update = None;
		self.dt_window.clear();
	}
	#[inline(always)]
	pub fn update(&mut self) -> TimePoint {
		let now = Instant::now();

		self.dt = self
			.last_update
			.map(|last| (now - last).as_secs_f32())
			.unwrap_or(0.0);

		self.dt_window.pop_front();
		self.dt_window.push_back(self.dt);

		self.last_update = Some(now);
		self.frame_count += 1;
		self.elapsed += self.dt;

		if self.frame_count % WINDOW_SIZE == 0 && self.frame_count != 0 {
			self.report_stats("time report");
		}

		TimePoint {
			dt: self.dt,
			elapsed: self.elapsed,
		}
	}
	#[inline]
	pub fn avg_frame_rate(&self) -> f32 {
		1.0 / self.avg_dt()
	}
	#[inline]
	pub fn avg_frame_rate_window(&self) -> f32 {
		1.0 / self.avg_dt_window()
	}
	#[inline]
	pub fn avg_dt(&self) -> f32 {
		self.elapsed / self.frame_count as f32
	}
	#[inline]
	pub fn avg_dt_window(&self) -> f32 {
		self.dt_window.iter().sum::<f32>() / self.dt_window.len() as f32
	}

	pub fn time_point(&self) -> TimePoint {
		TimePoint {
			dt: self.dt,
			elapsed: self.elapsed,
		}
	}
	#[inline]
	pub fn dt(&self) -> f32 {
		self.dt
	}
	#[inline]
	pub fn elapsed(&self) -> f32 {
		self.elapsed
	}
	#[inline]
	pub fn frame_count(&self) -> usize {
		self.frame_count
	}
	pub fn dt_window(&self) -> &VecDeque<f32> {
		&self.dt_window
	}

	pub fn report_stats(&self, label: impl AsRef<str>) {
		let elapsed = self.elapsed;
		let frame_count = self.frame_count;
		let avg_dt = self.avg_dt();
		let avg_dt_window = self.avg_dt_window();
		let avg_frame_rate = self.avg_frame_rate();
		let avg_frame_rate_window = self.avg_frame_rate_window();
		info!(
			elapsed,
			frame_count,
			window_size = WINDOW_SIZE,
			avg_dt,
			avg_dt_window,
			avg_frame_rate,
			avg_frame_rate_window,
			"{}",
			label.as_ref()
		);
	}
}

impl<const WINDOW_SIZE: usize> Drop for TimeTracker<WINDOW_SIZE> {
	fn drop(&mut self) {
		self.report_stats(
			"Frame time statistics over the lifetime of the program",
		);
	}
}
